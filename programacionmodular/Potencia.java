package programacionmodular;

import java.util.Scanner;

/*este programa visualiza en pantalla la potencia
 * de una base elevada a un exponente.
 */

public class Potencia {

	public static void main(String[] args) {
		int base = pedirBase();
		int expo = pedirExponente();
		int calcular = calcularPotencia(base, expo);
		verResultado(base, expo, calcular);
	}
		
		////////////////////////////////////
	
		public static int pedirBase() {
		Scanner entrada = new Scanner(System.in);
		System.out.println("Introduzca una base: ");
			return entrada.nextInt();		
			}
		
		/////////////////////////////////
			
		public static int pedirExponente()  {
		Scanner entrada = new Scanner(System.in);
		System.out.println("Introduzca un exponente: ");
		return entrada.nextInt();
		}
		
		//////////////////////////////////////
		
		public static int calcularPotencia(int base, int expo) {
		int resultado = 1;
		for(int i = 1; i <= expo; i++) {
			resultado = resultado * base;
		//Enviar a pantalla el resultado
		System.out.println("El resultado es " + resultado);
		}
		return resultado;
	}
////////////////////////////////////////////////////
	public static void verResultado(int base, int expo, int calc) {
		System.out.println(base + " elevado a " + expo + " = " + calc);
	}
}

