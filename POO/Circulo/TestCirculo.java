 package POO.Circulo;

public class TestCirculo {

	public static void main(String[] args) {
		
		//Crear objeto de la clase circulo con el constructor con parámetros
		
		Circulo miCirculo = new Circulo(10);
		double area = miCirculo.calcularSuperficie();
		System.out.println("el area es: " + area);
		
		
		
		//Crear objeto de la clase circulo con el constructor sin parametros,
		//con valor de radio por defecto
		Circulo suCirculo = new Circulo();
		double area1 = suCirculo.calcularSuperficie();
		System.out.println("el area2 es: " + area1);
		
		
		
		
		
		
		
		
		
		

	}

}
