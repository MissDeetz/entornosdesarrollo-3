package estructurasdecontrol;

import java.util.Scanner;

/*este programa visualiza el 
 * factorial de un n�mero
 */
public class Factorial {

	public static void main(String[] args) {
		
		Scanner entrada = new Scanner(System.in);
		System.out.println("Introduzca un n�mero");
		double num = entrada.nextInt();
		double factorial = 1;
		while( num != 0) {
			System.out.println("factorial " + factorial);
			factorial = factorial * num;
			num--;
		}
		System.out.println("El factorial es " + factorial);
	}

}
