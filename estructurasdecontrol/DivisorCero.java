package estructurasdecontrol;

import java.util.Scanner;

public class DivisorCero {

	public static void main(String[] args) {
		
		
		/*Este programa pide al usuario 2 n�meros float 
		*y muestra por pantalla su divisi�n.
		*Si el divisor es 0 el programa debe mostar un error.
		*/
		
		
		//Creamos un objeto de tipo scanner.
		
		
		Scanner entrada = new Scanner (System.in);
		
		//Introducimos por teclado los n�meros.
		

		System.out.println("Introduce el primer n�mero");
		double dividendo = entrada.nextInt();
		System.out.println("Introduce el segundo n�mero");
		double divisor = entrada.nextInt();
		
		
		
		if(divisor==0) {
			System.out.println("error, divisor no puede ser 0");
		}else {
			double resultado = dividendo / divisor;
			System.out.println(" La division de " +dividendo + " entre " +divisor + " es " + resultado);
					
			
			
		}
	}

}
