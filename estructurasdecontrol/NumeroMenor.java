package estructurasdecontrol;

import java.util.Scanner;

public class NumeroMenor {

	public static void main(String[] args) {

		
	/*Este programa muestra el numero menor 
	 * de dos cantidades	
	 */
		
		Scanner entrada = new Scanner (System.in);
		
		//Introducimos por teclado los n�meros
		
		System.out.println("Introduce el primer n�mero");
		int num1 = entrada.nextInt();
		System.out.println("Introduce el segundo n�mero");
		int num2 = entrada.nextInt();
		
		
		if(num1<num2){
			System.out.println("El n�mero menor es " +num1);
			
		}

		else if(num2<num1){
			System.out.println("El n�mero menor es " +num2);
			
		}
		
		else {
		System.out.println("Los n�meros son iguales");
	
		}
		
	}

}

